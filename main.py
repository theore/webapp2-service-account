#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
import os

import httplib2
import logging
import webapp2
from apiclient.discovery import build
from google.appengine.api import memcache
from oauth2client.contrib.appengine import AppAssertionCredentials
from oauth2client.service_account import ServiceAccountCredentials


SERVICE_ACCOUNT_JSON = os.path.join(os.path.dirname(__file__), 'service-account.json')


class MainHandler(webapp2.RequestHandler):
    def get(self):
        if os.environ.get('SERVER_SOFTWARE', 'Dev').startswith('Dev'):
            scopes = ['https://www.googleapis.com/auth/calendar']
            credentials = ServiceAccountCredentials.from_json_keyfile_name(
                SERVICE_ACCOUNT_JSON, scopes)
        else:
            credentials = AppAssertionCredentials(scope='https://www.googleapis.com/auth/calendar')

        calendar_id = '<calendar-id>'
        http = credentials.authorize(httplib2.Http(memcache))
        service = build('calendar', 'v3')
        events = service.events().list(
            calendarId=calendar_id,
            singleEvents=True, maxResults=250,
            orderBy='startTime').execute(http=http)

        calendars = service.calendarList().list().execute(http)
        logging.info(calendars)
        self.response.write(json.dumps(calendars))

        self.response.write(json.dumps(events))


app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)
